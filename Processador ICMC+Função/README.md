Nesta aba será encontrado os códigos do projeto de "Invasão ao banco" e o processador com a função fatorial implementada para a matéria de Introdução à Sistemas Computacionais da USP, além de um vídeo explicativo sobre o jogo e a nova função no processador.


Link do vídeo da função: https://www.youtube.com/watch?v=91mkg0eC1_k

Link do vídeo do jogo: https://youtu.be/VCQ2oj_tp1M

Link do compilador de linguagem C (adaptada) para assembly utilizado: https://github.com/icmc-ide 